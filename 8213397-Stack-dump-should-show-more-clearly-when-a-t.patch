From 3a774c78473c4fc3dcd1dc39f8c9daec4c5a6502 Mon Sep 17 00:00:00 2001
Date: Thu, 21 Sep 2023 14:45:54 +0800
Subject: 8213397-Stack-dump-should-show-more-clearly-when-a-t.patch

---
 hotspot/src/share/vm/oops/instanceKlass.cpp   |  23 +-
 hotspot/src/share/vm/runtime/thread.cpp       |   2 +
 hotspot/src/share/vm/runtime/thread.hpp       |   8 +
 .../src/share/vm/runtime/thread.inline.hpp    |  12 +
 hotspot/src/share/vm/runtime/vframe.cpp       |   8 +
 .../TestThreadDumpClassInitMonitor.java       | 217 ++++++++++++++++++
 6 files changed, 259 insertions(+), 11 deletions(-)
 create mode 100644 hotspot/test/runtime/Thread/TestThreadDumpClassInitMonitor.java

diff --git a/hotspot/src/share/vm/oops/instanceKlass.cpp b/hotspot/src/share/vm/oops/instanceKlass.cpp
index 538645bbe..993778270 100644
--- a/hotspot/src/share/vm/oops/instanceKlass.cpp
+++ b/hotspot/src/share/vm/oops/instanceKlass.cpp
@@ -920,25 +920,28 @@ void InstanceKlass::initialize_impl(instanceKlassHandle this_oop, TRAPS) {
 
   bool wait = false;
 
+  assert(THREAD->is_Java_thread(), "non-JavaThread in initialize_impl");
+  JavaThread* jt = (JavaThread*)THREAD;
+
   // refer to the JVM book page 47 for description of steps
   // Step 1
   {
     oop init_lock = this_oop->init_lock();
     ObjectLocker ol(init_lock, THREAD, init_lock != NULL);
 
-    Thread *self = THREAD; // it's passed the current thread
-
     // Step 2
     // If we were to use wait() instead of waitInterruptibly() then
     // we might end up throwing IE from link/symbol resolution sites
     // that aren't expected to throw.  This would wreak havoc.  See 6320309.
-    while(this_oop->is_being_initialized() && !this_oop->is_reentrant_initialization(self)) {
-        wait = true;
-      ol.waitUninterruptibly(CHECK);
+    while (this_oop->is_being_initialized() && !this_oop->is_reentrant_initialization(jt)) {
+      wait = true;
+      jt->set_class_to_be_initialized(this_oop());
+      ol.waitUninterruptibly(jt);
+      jt->set_class_to_be_initialized(NULL);
     }
 
     // Step 3
-    if (this_oop->is_being_initialized() && this_oop->is_reentrant_initialization(self)) {
+    if (this_oop->is_being_initialized() && this_oop->is_reentrant_initialization(jt)) {
       DTRACE_CLASSINIT_PROBE_WAIT(recursive, InstanceKlass::cast(this_oop()), -1,wait);
       return;
     }
@@ -968,7 +971,7 @@ void InstanceKlass::initialize_impl(instanceKlassHandle this_oop, TRAPS) {
 
     // Step 6
     this_oop->set_init_state(being_initialized);
-    this_oop->set_init_thread(self);
+    this_oop->set_init_thread(jt);
   }
 
   // Step 7
@@ -1004,8 +1007,6 @@ void InstanceKlass::initialize_impl(instanceKlassHandle this_oop, TRAPS) {
 
   // Step 8
   {
-    assert(THREAD->is_Java_thread(), "non-JavaThread in initialize_impl");
-    JavaThread* jt = (JavaThread*)THREAD;
     DTRACE_CLASSINIT_PROBE_WAIT(clinit, InstanceKlass::cast(this_oop()), -1,wait);
     // Timer includes any side effects of class initialization (resolution,
     // etc), but not recursive entry into call_class_initializer().
@@ -1031,14 +1032,14 @@ void InstanceKlass::initialize_impl(instanceKlassHandle this_oop, TRAPS) {
     CLEAR_PENDING_EXCEPTION;
     // JVMTI has already reported the pending exception
     // JVMTI internal flag reset is needed in order to report ExceptionInInitializerError
-    JvmtiExport::clear_detected_exception((JavaThread*)THREAD);
+    JvmtiExport::clear_detected_exception(jt);
     {
       EXCEPTION_MARK;
       this_oop->set_initialization_state_and_notify(initialization_error, THREAD);
       CLEAR_PENDING_EXCEPTION;   // ignore any exception thrown, class initialization error is thrown below
       // JVMTI has already reported the pending exception
       // JVMTI internal flag reset is needed in order to report ExceptionInInitializerError
-      JvmtiExport::clear_detected_exception((JavaThread*)THREAD);
+      JvmtiExport::clear_detected_exception(jt);
     }
     DTRACE_CLASSINIT_PROBE_WAIT(error, InstanceKlass::cast(this_oop()), -1,wait);
     if (e->is_a(SystemDictionary::Error_klass())) {
diff --git a/hotspot/src/share/vm/runtime/thread.cpp b/hotspot/src/share/vm/runtime/thread.cpp
index 2be226463..50543ac73 100644
--- a/hotspot/src/share/vm/runtime/thread.cpp
+++ b/hotspot/src/share/vm/runtime/thread.cpp
@@ -1538,6 +1538,8 @@ void JavaThread::initialize() {
   _popframe_preserved_args_size = 0;
   _frames_to_pop_failed_realloc = 0;
 
+_class_to_be_initialized = NULL;
+
   pd_initialize();
 }
 
diff --git a/hotspot/src/share/vm/runtime/thread.hpp b/hotspot/src/share/vm/runtime/thread.hpp
index 220fe9316..1d3caf9aa 100644
--- a/hotspot/src/share/vm/runtime/thread.hpp
+++ b/hotspot/src/share/vm/runtime/thread.hpp
@@ -1802,11 +1802,19 @@ public:
   bool is_attaching_via_jni() const { return _jni_attach_state == _attaching_via_jni; }
   bool has_attached_via_jni() const { return is_attaching_via_jni() || _jni_attach_state == _attached_via_jni; }
   inline void set_done_attaching_via_jni();
+
+  // Stack dump assistance: Track the class we want to initialize but for which we have to wait
+  // on its init_lock() because it is already being initialized.
+  inline void set_class_to_be_initialized(InstanceKlass* k);
+  inline InstanceKlass* class_to_be_initialized() const;
+
 private:
   // This field is used to determine if a thread has claimed
   // a par_id: it is UINT_MAX if the thread has not claimed a par_id;
   // otherwise its value is the par_id that has been claimed.
   uint _claimed_par_id;
+
+  InstanceKlass* _class_to_be_initialized;
 public:
   uint get_claimed_par_id() { return _claimed_par_id; }
   void set_claimed_par_id(uint id) { _claimed_par_id = id;}
diff --git a/hotspot/src/share/vm/runtime/thread.inline.hpp b/hotspot/src/share/vm/runtime/thread.inline.hpp
index b05e0ec5c..d3ab3aba9 100644
--- a/hotspot/src/share/vm/runtime/thread.inline.hpp
+++ b/hotspot/src/share/vm/runtime/thread.inline.hpp
@@ -74,4 +74,16 @@ inline void JavaThread::set_done_attaching_via_jni() {
   OrderAccess::fence();
 }
 
+// Allow tracking of class initialization monitor use
+inline void JavaThread::set_class_to_be_initialized(InstanceKlass* k) {
+  assert((k == NULL && _class_to_be_initialized != NULL) ||
+         (k != NULL && _class_to_be_initialized == NULL), "incorrect usage");
+  assert(this == Thread::current(), "Only the current thread can set this field");
+  _class_to_be_initialized = k;
+}
+
+inline InstanceKlass* JavaThread::class_to_be_initialized() const {
+  return _class_to_be_initialized;
+}
+
 #endif // SHARE_VM_RUNTIME_THREAD_INLINE_HPP
diff --git a/hotspot/src/share/vm/runtime/vframe.cpp b/hotspot/src/share/vm/runtime/vframe.cpp
index b3a6d0770..93d62ade7 100644
--- a/hotspot/src/share/vm/runtime/vframe.cpp
+++ b/hotspot/src/share/vm/runtime/vframe.cpp
@@ -176,6 +176,14 @@ void javaVFrame::print_lock_info_on(outputStream* st, int frame_count) {
       Klass* k = obj->klass();
       st->print_cr("\t- %s <" INTPTR_FORMAT "> (a %s)", "parking to wait for ", (address)obj, k->external_name());
     }
+    else if (thread()->osthread()->get_state() == OBJECT_WAIT) {
+      // We are waiting on an Object monitor but Object.wait() isn't the
+      // top-frame, so we should be waiting on a Class initialization monitor.
+      InstanceKlass* k = thread()->class_to_be_initialized();
+      if (k != NULL) {
+        st->print_cr("\t- waiting on the Class initialization monitor for %s", k->external_name());
+      }
+    }
   }
 
 
diff --git a/hotspot/test/runtime/Thread/TestThreadDumpClassInitMonitor.java b/hotspot/test/runtime/Thread/TestThreadDumpClassInitMonitor.java
new file mode 100644
index 000000000..8aa218efb
--- /dev/null
+++ b/hotspot/test/runtime/Thread/TestThreadDumpClassInitMonitor.java
@@ -0,0 +1,217 @@
+/*
+ * Copyright (c) 2019, Oracle and/or its affiliates. All rights reserved.
+ * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
+ *
+ * This code is free software; you can redistribute it and/or modify it
+ * under the terms of the GNU General Public License version 2 only, as
+ * published by the Free Software Foundation.
+ *
+ * This code is distributed in the hope that it will be useful, but WITHOUT
+ * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
+ * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
+ * version 2 for more details (a copy is included in the LICENSE file that
+ * accompanied this code).
+ *
+ * You should have received a copy of the GNU General Public License version
+ * 2 along with this work; if not, write to the Free Software Foundation,
+ * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
+ *
+ * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
+ * or visit www.oracle.com if you need additional information or have any
+ * questions.
+ */
+
+/*
+ * @test
+ * @bug     8213397 8217337
+ * @summary Check that the thread dump shows when a thread is blocked
+ *          on a class initialization monitor
+ *
+ * @library /testlibrary
+ * @run main/othervm TestThreadDumpClassInitMonitor
+ */
+
+import com.oracle.java.testlibrary.*;
+
+import java.io.IOException;
+import java.util.List;
+import java.lang.management.ManagementFactory;
+import java.lang.management.RuntimeMXBean;
+
+public class TestThreadDumpClassInitMonitor {
+    // jstack tends to be closely bound to the VM that we are running
+    // so use getTestJDKTool() instead of getCompileJDKTool() or even
+    // getJDKTool() which can fall back to "compile.jdk".
+    final static String JSTACK = JDKToolFinder.getTestJDKTool("jstack");
+    final static String PID = getPid();
+    final static Thread current = Thread.currentThread();
+
+    /*
+     * This is the output we're looking for:
+     *
+     * "TestThread" #22 prio=5 os_prio=0 cpu=1.19ms elapsed=0.80s tid=0x00007f8f9405d800 nid=0x568b in Object.wait()  [0x00007f8fd94d0000]
+     *   java.lang.Thread.State: RUNNABLE
+     * Thread: 0x00007f8f9405d800  [0x568b] State: _at_safepoint _has_called_back 0 _at_poll_safepoint 0  // DEBUG ONLY
+     *   JavaThread state: _thread_blocked                                                                // DEBUG ONLY
+     *         at TestThreadDumpClassInitMonitor$Target$1.run(TestThreadDumpClassInitMonitor.java:69)
+     *         - waiting on the Class initialization monitor for TestThreadDumpClassInitMonitor$Target
+     *
+     */
+    final static String TEST_THREAD = "TestThread";
+    final static String TEST_THREAD_ENTRY = "\"" + TEST_THREAD;
+    final static String IN_OBJECT_WAIT = "in Object.wait()";
+    final static String THREAD_STATE = "java.lang.Thread.State: RUNNABLE";
+    final static String THREAD_INFO = "Thread:"; // the details are not important
+    final static String JAVATHREAD_STATE = "JavaThread state: _thread_blocked";
+    final static String CURRENT_METHOD = "at TestThreadDumpClassInitMonitor$Target$1.run";
+    final static String WAIT_INFO = "- waiting on the Class initialization monitor for TestThreadDumpClassInitMonitor$Target";
+
+    volatile static boolean ready = false;
+
+    static List<String> stackDump;  // jstack output as lines
+
+    static class Target {
+
+        static int field;
+
+        // The main thread will initialize this class and so
+        // execute the actual test logic here.
+        static {
+            if (Thread.currentThread() != current) {
+                throw new Error("Initialization logic error");
+            }
+            System.out.println("Initializing Target class in main thread");
+
+            Thread t  = new Thread() {
+                    public void run() {
+                        System.out.println("Test thread about to access Target");
+                        ready = true; // tell main thread we're close
+                        // This will block until the main thread completes
+                        // static initialization of target
+                        Target.field = 42;
+                        System.out.println("Test thread done");
+                    }
+                };
+            t.setName(TEST_THREAD);
+            t.start();
+
+            // We want to run jstack once the test thread is blocked but
+            // there's no programmatic way to detect that. So we check the flag
+            // that will be set just before it should block, then by the time
+            // we can exec jstack it should be ready.
+            try {
+                while (!ready) {
+                    Thread.sleep(200);
+                }
+            }
+            catch (InterruptedException ie) {
+                throw new Error("Shouldn't happen");
+            }
+
+            // Now run jstack
+            try {
+                ProcessBuilder pb = new ProcessBuilder(JSTACK, PID);
+                OutputAnalyzer output = new OutputAnalyzer(pb.start());
+                output.shouldHaveExitValue(0);
+                stackDump = output.asLines();
+            }
+            catch (IOException ioe) {
+                throw new Error("Launching jstack failed", ioe);
+            }
+        }
+    }
+
+
+    public static void main(String[] args) throws Throwable {
+        // Implicitly run the main test logic
+        Target.field = 21;
+
+        // Now check the output of jstack
+        try {
+            // product builds miss 2 lines of information in the stack
+            boolean isProduct = !Platform.isDebugBuild();
+            int foundLines = 0;
+            parseStack: for (String line : stackDump) {
+                switch(foundLines) {
+                case 0: {
+                    if (!line.startsWith(TEST_THREAD_ENTRY)) {
+                        continue;
+                    }
+                    foundLines++;
+                    if (!line.contains(IN_OBJECT_WAIT)) {
+                        throw new Error("Unexpected initial stack line: " + line);
+                    }
+                    continue;
+                }
+                case 1: {
+                    if (!line.trim().equals(THREAD_STATE)) {
+                        throw new Error("Unexpected thread state line: " + line);
+                    }
+                    if (isProduct) {
+                        foundLines += 3;
+                    } else {
+                        foundLines++;
+                    }
+                    continue;
+                }
+                case 2: { // Debug build
+                    if (!line.startsWith(THREAD_INFO)) {
+                        throw new Error("Unexpected thread info line: " + line);
+                    }
+                    foundLines++;
+                    continue;
+                }
+                case 3: { // Debug build
+                    if (!line.trim().equals(JAVATHREAD_STATE)) {
+                        throw new Error("Unexpected JavaThread state line: " + line);
+                    }
+                    foundLines++;
+                    continue;
+                }
+                case 4: {
+                    if (!line.trim().startsWith(CURRENT_METHOD)) {
+                        throw new Error("Unexpected current method line: " + line);
+                    }
+                    foundLines++;
+                    continue;
+                }
+                case 5: {
+                    if (!line.trim().equals(WAIT_INFO)) {
+                        throw new Error("Unexpected monitor information line: " + line);
+                    }
+                    break parseStack;
+                }
+                default: throw new Error("Logic error in case statement");
+                }
+            }
+
+            if (foundLines == 0) {
+                throw new Error("Unexpected stack content - did not find line starting with "
+                                + TEST_THREAD_ENTRY);
+            }
+        }
+        catch (Error e) {
+            // Dump the full stack trace on error so we can check the content
+            for (String line : stackDump) {
+                System.out.println(line);
+            }
+            throw e;
+        }
+    }
+
+    // This helper relies on RuntimeMXBean.getName() returning a string
+    // that looks like this: 5436@mt-haku
+    //
+    // The testlibrary has tryFindJvmPid(), but that uses a separate
+    // process which is much more expensive for finding out your own PID.
+    //
+    static String getPid() {
+        RuntimeMXBean runtimebean = ManagementFactory.getRuntimeMXBean();
+        String vmname = runtimebean.getName();
+        int i = vmname.indexOf('@');
+        if (i != -1) {
+            vmname = vmname.substring(0, i);
+        }
+        return vmname;
+    }
+}
\ No newline at end of file
-- 
2.22.0

